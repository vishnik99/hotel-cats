// Main js file
// see more: https://github.com/vedees/webpack-template/blob/master/README.md#import-js-files
import $ from 'jquery';
import 'slick-carousel';
$(".rooms").slick({
    infinite: true,
    dots: true,
    dotsClass: 'dots',
    appendArrows: $('.arrows_rooms'),
    nextArrow: '<button type="button" class="arrow arrow_next">›</button>',
    prevArrow: '<button type="button" class="arrow arrow_prev">‹</button>',
    appendDots: $('.dots_rooms'),
});

$(".reviews").slick({
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    dots: true,
    dotsClass: 'dots',
    appendArrows: $('.arrows_reviews'),
    nextArrow: '<button type="button" class="arrow arrow_next">›</button>',
    prevArrow: '<button type="button" class="arrow arrow_prev">‹</button>',
    appendDots: $('.dots_reviews'),
    adaptiveHeight: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                adaptiveHeight: true,
                centerMode: true,
                centerPadding: '10%',
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 992,
            settings: {
                arrows: false,
                adaptiveHeight: true,
                centerMode: true,
                centerPadding: '15%',
                slidesToShow: 1,
            }
        },
        {
            breakpoint: 768,
            settings: {
                adaptiveHeight: true,
                centerMode: true,
                centerPadding: '1%',
                slidesToShow: 1,
            }
        },
        {
            breakpoint: 540,
            settings: {
                adaptiveHeight: true,
                centerMode: false,
                centerPadding: '1%',
                slidesToShow: 1,
            }
        }
    ]
});

/*
 * Отменяем действие по умолчанию для галлереи
 * */

const roomCard = document.getElementById('js-room-card');
if(roomCard) {
    roomCard.addEventListener('click', (e) => {
        const addImg = e.target;
        if (!addImg.closest('.room__sm-img')) return;

        const mainImg = roomCard.querySelector('.room__bg-img > img');
        const mainImgSrc = mainImg.getAttribute('src');
        const mainImgSmSrc = mainImg.dataset.smImg;

        const addImgSrc = addImg.getAttribute('src');
        const addImgBgSrc = addImg.dataset.bgImg;

        mainImg.setAttribute('src', addImgBgSrc);
        mainImg.setAttribute('data-sm-img', addImgSrc);

        addImg.setAttribute('src', mainImgSmSrc);
        addImg.setAttribute('data-bg-img', mainImgSrc);
    });
}

// Кнопка для сброса фильтра
const dropFiltersBtn =  document.getElementById('js-drop-filters');
if(dropFiltersBtn) {
    dropFiltersBtn.addEventListener('click', (e) => {
        const filtersContainer = e.target.closest('#js-filters-container');
        const inputs = filtersContainer.querySelectorAll('input');
        inputs.forEach((input) => {
            input.checked = false;
            input.value = "";
        });
    });
}

// Показываем подсказки при наводе на иконку

const roomCatalog = document.getElementById('js-catalog-rooms');
if(roomCatalog) {
    roomCatalog.addEventListener('mouseover', (e) => {
        const target = e.target;
        if (!target.classList.contains('facilities-elem__icon')) return;
        const tip = document.createElement('span');
        const tipParent = target.parentElement;
        const roomCard = tipParent.closest('.catalog-room');
        const roomCardCoords = roomCard.getBoundingClientRect();
        tip.innerHTML = tipParent.querySelector('.facilities-elem__title').innerHTML;
        tip.classList.add("facilities-elem__tip");
        tipParent.append(tip);
        tipParent.classList.toggle('tip');
        const tipCoords = tip.getBoundingClientRect();
        let shiftX = 15;
        if (tipCoords.right > roomCardCoords.right) {
            shiftX += tipCoords.right - roomCardCoords.right;
        }
        tip.style.top = -(tip.offsetHeight + 20) + "px";
        tip.style.left = -shiftX + 'px';
    });

    roomCatalog.addEventListener('mouseout', (e) => {
        const target = e.target;
        if (!target.classList.contains('facilities-elem__icon')) return;
        const tipParent = target.parentElement;
        const tip = tipParent.querySelector('.facilities-elem__tip');
        if (tip) {
            tipParent.classList.toggle('tip');
            tip.remove();
        }
    });
}

// Событие для фильтрации каталога
const filterForm = document.getElementById('js-filters-container');
if(filterForm) {
    const rooms = roomCatalog.querySelectorAll('.catalog-room');
    filterForm.addEventListener('submit', (e) => {
        e.preventDefault();
    });
    filterForm.addEventListener('change', (e) => {
        // Получаем данные с формы
        //console.log(filterForm.elements);
        const startPrice = parseInt(filterForm.elements['start-price'].value) || 0;
        const endPrice = parseInt(filterForm.elements['end-price'].value) || 999999;
        console.log(startPrice);
        console.log(endPrice);
        const roomFacilities = [];
        const roomSquares = [];
        const formElements = filterForm.elements;
        for (let i = 0; i < formElements.length; i++) {
            const elem = formElements[i];
            if (elem.classList.contains('checkbox__input_square')) {
                if (!elem.checked) continue;
                roomSquares.push(parseFloat(elem.value));
            } else if (elem.classList.contains('checkbox__input_facilities')) {
                if (!elem.checked) continue;
                roomFacilities.push(elem.value);
            }
        }

        // Изменяем список комнат на странице

        // Получаем список комнат на странице
        const roomCatalog = document.getElementById('js-catalog-rooms');
        const srcRooms = Array.prototype.slice.apply(rooms);
        console.log(roomSquares);
        const suitRooms = Array.prototype.filter.call(srcRooms, (room, i, srcRooms) => {
            const roomPrice = parseInt(room.querySelector('.room-descr__price').innerHTML);
            const roomSquare = parseFloat(room.querySelector('.room__area').innerHTML.replace(/,/g, '.'));
            console.log(roomSquare);
            if (roomPrice < startPrice || roomPrice > endPrice) {
                return false;
            }
            if (roomSquares.length !== 0) {
                if (roomSquares.indexOf(roomSquare) === -1) {
                    return false;
                }
            }
            return true;
        });
        console.log(srcRooms);
        roomCatalog.innerHTML = "";
        for (let i = 0; i < suitRooms.length; i++) {
            roomCatalog.append(suitRooms[i]);
        }
    });
}

// Работа с popup

document.addEventListener('click', (e) => {
   if(!e.target.classList.contains('open-popup') && !e.target.classList.contains('popup__close-btn')) return;
   const popup = document.getElementById('js-popup');
   popup.classList.toggle('popup_active');
});

// Работа с выпадающим меню
document.addEventListener('click', (e)=> {
   if(!e.target.closest('.burger-btn')) return;
   const burgerBtn = e.target.closest('.burger-btn');
   const menu = burgerBtn.parentElement.querySelector('.menu');
   menu.classList.toggle('menu_open_bottom');
   burgerBtn.classList.toggle('burger-btn_close');

});

